package com.vc.im.model.service;

import java.util.List;

import com.vc.im.model.entity.ThirdPartyUser;

public interface IThirdPartyUserService
{
	List<ThirdPartyUser> findAll();

	ThirdPartyUser save(ThirdPartyUser user);
}
