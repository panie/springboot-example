package com.vc.im.model.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vc.im.model.entity.ThirdPartyUser;
import com.vc.im.model.service.IThirdPartyUserService;

@RestController
@RequestMapping("/")
public class ThirdPartyUserController
{
	@Autowired
	private IThirdPartyUserService thirdPartyUserService;

	/**
	 * 
	 * @return
	 */
	@RequestMapping("getList")
	@ResponseBody
	public List<ThirdPartyUser> getList()
	{
		System.out.println("11111111111");
		List<ThirdPartyUser> thirdPartyUsrtList = thirdPartyUserService.findAll();
		return thirdPartyUsrtList;
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ThirdPartyUser add(ThirdPartyUser user)
	{
		return thirdPartyUserService.save(user);
	}

}
