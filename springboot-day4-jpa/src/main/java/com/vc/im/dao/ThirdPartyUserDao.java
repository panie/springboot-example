package com.vc.im.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vc.im.model.entity.ThirdPartyUser;

public interface ThirdPartyUserDao extends JpaRepository<ThirdPartyUser, Integer>
{

}
