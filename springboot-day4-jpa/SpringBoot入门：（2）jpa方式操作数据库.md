#### 导入jar ，在pom.xml中添加依赖:
```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>

<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
</dependency>
```

#### 在appilication.yml中添加数据库配置：
```
spring:
  profiles:
    active: dev

  datasource:
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://localhost:3306/dbvc?useUnicode=true&characterEncoding=utf8&characterSetResults=utf8
    username: root
    password: 123

  jpa:
    hibernate:
      ddl-auto: create
    show-sql: true
```
注：ddl_auto: create 代表在数据库创建表，update 代表更新，首次启动需要create ,如果你想通过hibernate 注解的方式创建数据库的表的话，之后需要改为 update.
spring:
  profiles:
    active: 用来表示多配置文件时，选择哪一个配置文件


#### 创建一个实体
```
package com.vc.im.model.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ThirdPartyUser
{
	@Id
	@GeneratedValue
	private Integer id;

	private String name;

	private String openid;

	private String accessToken;

	private String refreshToken;

	private String mediaType;

	private Date createTime;

	private Date updateTime;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name == null ? null : name.trim();
	}

	public String getOpenid()
	{
		return openid;
	}

	public void setOpenid(String openid)
	{
		this.openid = openid == null ? null : openid.trim();
	}

	public String getAccessToken()
	{
		return accessToken;
	}

	public void setAccessToken(String accessToken)
	{
		this.accessToken = accessToken == null ? null : accessToken.trim();
	}

	public String getRefreshToken()
	{
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken)
	{
		this.refreshToken = refreshToken == null ? null : refreshToken.trim();
	}

	public String getMediaType()
	{
		return mediaType;
	}

	public void setMediaType(String mediaType)
	{
		this.mediaType = mediaType == null ? null : mediaType.trim();
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}
}
```

####  创建Dao接口, springboot 将接口类会自动注解到spring容器中，不需要我吗做任何配置，只需要继承JpaRepository 即可

```
package com.vc.im.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vc.im.model.entity.ThirdPartyUser;

public interface ThirdPartyUserDao extends JpaRepository<ThirdPartyUser, Integer>
{

}

```

####  创建一个service层,接口和实现类.这里省略，就是把Dao 封装一下。在实现类上 加上 @Service 注解
如果需要事务的话，在service层加@Transaction注解即可。
……

####  Controller 层

```
package com.vc.im.model.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vc.im.model.entity.ThirdPartyUser;
import com.vc.im.model.service.IThirdPartyUserService;

@RestController
@RequestMapping("/")
public class ThirdPartyUserController
{
	@Autowired
	private IThirdPartyUserService thirdPartyUserService;

	/**
	 * 
	 * @return
	 */
	@RequestMapping("getList")
	@ResponseBody
	public List<ThirdPartyUser> getList()
	{
		System.out.println("11111111111");
		List<ThirdPartyUser> thirdPartyUsrtList = thirdPartyUserService.findAll();
		return thirdPartyUsrtList;
	}

	/**
	 * 
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ThirdPartyUser add(ThirdPartyUser user)
	{
		return thirdPartyUserService.save(user);
	}

}

```

### 启动类
```
package com.vc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@SpringBootApplication
public class TestController  {
 
    @ResponseBody
    @RequestMapping(value = "/")
    String home() {   
        return "Hello World!";
    }
 
    public static void main(String[] args) throws Exception {
        SpringApplication.run(TestController.class, args);
    }
 
}
```


参考:[2小时学会springboot](http://blog.csdn.net/forezp/article/details/61472783)