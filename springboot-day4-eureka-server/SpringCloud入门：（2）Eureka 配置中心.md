
> 前言：  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  在整理blog之前，请允许我吐个槽。又折腾了一天，还好至少出了个结果了。因为直接上实例实现是无法知道这段代码是什么含义，多个配置项、少个配置项又有什么影响，于是不得不又去搜了很多的blog来了解一些理论知识。   
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  现在还是没法将这些理论知识描述出来，但至少有个意会在心了。   
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   我想就算是同一套理论，不同的人看起来最后理解的都会有些差异吧，就像武功秘籍，每个人学会的招式都有差异的。

#### 理论
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  还先考虑一下，我这一章是想要做什么？   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  嗯，
前一章提到 一个服务（config-client）是可以从配置中心（config-server）读取文件，而配置中心是从git读取的配置文件。当服务实例很多时，若都是从配置中心读取文件，可以考虑将配置中心做成微服务，将其集群化，从而达到高可用。而SpringCloud 提供了 Eureka 可以方便快捷的达到这个功能。   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  故，我想要尝试一下 采用了Eureka 服务发现来实现配置文件的集中管理 这个功能。


**先了解一下 Eureka**  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  当调用API 或者 发起网络通信的时候，我们一定要知道 对应的IP 和端口。但在微服务中，尤其是使用了Docker 等虚拟化技术的微服务，其IP 和Port 都是动态分配的，服务实例数也是动态变化的，就需要精细而准确的服务发现机制。当微服务app启动后，告诉其他服务（Eureka Server 和 Eureka client）自己的ip和端口。


&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Eureka 属于 客户端发现模式，客户端负责决定服务实例的网络位置，并且对请求实现负载均衡。客户端从一个服务注册服务中查询所有可用服务实例的库，并缓存到本地。
服务调用时，客户端使用负载均衡算法从多个后端服务实例中选择出一个，然后发出请求。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  Eureka 分为 Eureka Server 和 Eureka Client，Eureka Server 是一个服务注册中心，为服务实例注册管理和查询可用实例提供了 REST API，并可以用其定位、负载均衡、故障恢复后端服务的中间层服务。在服务启动后，Eureka Client 向服务注册中心注册服务同时会拉取注册中心注册表副本；在服务停止的时候，Eureka Client 向服务注册中心注销服务；服务注册后，Eureka Client 会定时的发送心跳来刷新服务的最新状态。

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
eureka服务： 用以服务发现、服务注册  
eureka-server: 相对client端的服务端，为客户端提供服务，通常情况下为一个集群  
eureka-client:客户端，通过向eureka服务发现注册的可用的eureka-server，向后端发送请求

**spring cloud eureka**

spring cloud eureka 分为两部分  
@EnableEurekaClient: 该注解表明应用既作为eureka实例又为eureka client 可以发现注册的服务  
@EnableEurekaServer: 该注解表明应用为eureka服务，有可以联合多个服务作为集群，对外提供服务注册以及发现功能   

> 额，我也不知道这段话是啥意思了，感觉 很重要，就先抄过来了。

#### 任务
在上一章任务中基础上修改文件即可

创建配置管理服务器及实现分布式配置管理应用
- springboot-config-repo：配置文件存放的文件夹 （不变）
- springboot-day4-eureka-server ：Eureka 用来发现服务 （新建）
- springboot-day4-config-server ：服务端：管理配置文件 （修改）
- springboot-day4-config-client ：客户端：使用配置文件 （修改）


### spring cloud分布式配置文件 步骤
#### 1. 创建springboot-config-repo项目
当配置服务器创建好后，将可以用http 请求访问到这些配置文件的内容

http请求地址和资源文件映射如下:
```
/{application}/{profile}[/{label}]  
/{application}-{profile}.yml
/{label}/{application}-{profile}.yml
/{application}-{profile}.properties
/{label}/{application}-{profile}.properties
```
- {application} 对应着服务端中 `application.* `和 客户端中`bootstrap.*` 文件中 对应的“spring.application.name”;
- {profile} 则对应客户端中`bootstrap.*` 文件中 对应的“spring.active”  
- {label} 则是一个服务器端功能标签“版本”的（即git 的版本，默认master)

在这里，我创建 三个文件 
```
application.properties
config-client-dev.properties
config-server-dev.properties
```
分别在对应文件 中添加内容为 
```
foo=application
foo=config-client
foo=config-server
```
将此文件提交到本地的git 中
```
echo foo=application > application.properties
echo foo=config-client > config-client-dev.properties
echo foo=config-server > config-server-dev.properties
git init
git add .
git commit -m "config init"
```
为后面演示做示例

**Spring cloud使用git或svn存放配置文件，默认情况下使用git.因此你需要安装git私服或者直接使用互联网上的github或者git.oschina，这里推荐使用git.oschina。**

#### 2. Eureka Server


-  创建一个空的maven web项目，如 eureka-server
- 添加一个类
```
package cloud.config.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EnrekaServerApplication
{

	public static void main(String[] args)
	{
		SpringApplication.run(EnrekaServerApplication.class, args);
	}
}

```

> 关于几个注解：  
>  @Configuration
@ComponentScan
@EnableAutoConfiguration 在功能上和 @SpringBootApplication 有一样的作用。  
@EnableEurekaServer 激活该应用为配置文件服务器.

- 修改配置文件 application.properties
```
server.port=8010
spring.profiles.active=dev
spring.application.name=eureka-server
eureka.instance.hostname=master
eureka.client.registerWithEureka=false
eureka.client.fetchRegistry=false
eureka.client.serviceUrl.defaultZone=http://${eureka.instance.hostname}:${server.port}/eureka/
```
- 其中:   
    - server.port是配置当前web应用绑定8888端口，   
    - defaultZone 为任意一个client提供一个服务url 。 默认的应用名（service ID），虚拟的主机名，端口 将会从环境变量中获取，分别是 ${spring.application.name}, ${spring.application.name} 和 ${server.port}
    - eureka.instance.hostname  填写本地电脑的 主机名
    - eureka.client.registerWithEureka 和 eureka.client.fetchRegistry 为 true 时，表示注册到 Eureka 中

- 启动服务。访问http://localhost:8010/ 可以看到 Eureka首页
- 
#### 2.修改 原有的 config-server 项目

- 修改配置文件 application.properties
```
server.port=8888
spring.application.name=config-server
spring.profiles.active=dev

spring.cloud.config.server.git.uri=file:///D:/workspace_eclipse_vs_git2/springboot-config-repo/
spring.cloud.config.server.git.searchPaths=springboot-config-repo
spring.cloud.config.label=master

eureka.client.serviceUrl.defaultZone=http://localhost:8010/eureka/
```
> 新增了一个配置项 eureka.client.serviceUrl.defaultZone 指明Eureka Server中的地址

- 修改pom 文件，加入依赖
```
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-eureka</artifactId>
</dependency>
```

- 启动服务，配置服务器即搭建完成。
    - 访问 http://localhost:8888/config-server/dev
    - 访问 http://localhost:8888/config-server/default
    - http://localhost:8888/config-client/dev
即会发现，结果还是和前一个项目一样的。

#### 3. 修改原有的 config-client


- 在pom.xml 引入依赖
```
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-eureka</artifactId>
</dependency>
```
- 配置文件 bootstrap.*
```
server.port=8011
spring.application.name=config-client
spring.cloud.config.label=master
spring.cloud.config.profile=dev
#spring.cloud.config.uri= http://localhost:8888/

eureka.client.serviceUrl.defaultZone=http://localhost:8010/eureka/
spring.cloud.config.discovery.enabled=true
spring.cloud.config.discovery.serviceId=config-server

```
> 这里配置文件进行了修改，原本直接访问 config-server ，现在通过eureka 来访问
> server.port=${PORT:${SERVER_PORT:0}} 可以随机指定个没使用的端口   
- 启动项目，访问 http://localhost:8011/hello 即可看到结果。结果正确与否，自己看吧。



> 注意：启动顺序为 eureka-server / config-server / config-client


#### 参考链接
[Spring-cloud-netflix](http://cloud.spring.io/spring-cloud-static/spring-cloud-netflix/1.3.1.RELEASE/)   
[Eureka服务发现 理论](http://www.jianshu.com/p/4136f5567606)   
[史上最简单的SpringCloud教程 | 第七篇: 高可用的分布式配置中心(Spring Cloud Config)](http://blog.csdn.net/forezp/article/details/70037513)