package cloud.config.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class EnrekaServerApplication
{

	public static void main(String[] args)
	{
		//SpringApplication.run(EnrekaServerApplication.class, args);
		new SpringApplicationBuilder(EnrekaServerApplication.class).web(true).run(args);
	}
}
