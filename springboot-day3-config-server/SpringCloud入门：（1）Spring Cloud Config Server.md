
> 前言  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;最近遇到个项目，总是不知道配置文件在哪，于是干脆把我不认识的技术作为关键字去搜索了一番。感谢开源、感谢网友的无私分享。经过了多次的尝试，甚至官方文档都抠出来了，终于做出来了像网友所描述的效果。  
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在这里把我所遇到的坑记录下来，方便自己也方便他人。

#### 理论
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;分布式配置管理是分布式系统和微服务应用的第一步。目的是把各个应用的配置信息集中管理。    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;因为一般上服务会分为开发、测试、生产等不同的维度。很容易导致开发环境与生产环境配置信息的不一致，或者管理复杂。  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Spring 配置管理模块，可以把一些启动参数进行外部配置。
要使用spring cloud分布式配置文件总体上分为3个大的步骤，首选你需要创建存放配置文件的仓库，然后创建一个配置文件服务器，该服务器将配置文件信息转化为rest接口数据，然后创建一个应用服务，该服务演示使用分布式配置文件信息   

![输入图片说明](https://git.oschina.net/uploads/images/2017/0930/093810_df21e4be_518135.jpeg "20170929_1.jpg")
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;在做每一件事的时候，都有个坑，那就是 为什么要这么做呢？好处是什么？ 回答不出这个问题就找不到去做的动力和意义。  
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;我也去找了下答案。   
>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;大致理由就是：分布式项目，每个项目都会有很多的配置文件啊，那些数据库连接配置啊、国际化配置啊 之类的，很容易出现配置文件的错误，尤其是环境不同（比如 测试、开发、生产），文件不统一的情况。

#### 任务
创建配置管理服务器及实现分布式配置管理应用
- springboot-config-repo：配置文件存放的文件夹
- springboot-day3-config-server ：服务端：管理配置文件
- springboot-day3-config-client ：客户端：使用配置文件


### spring cloud分布式配置文件 步骤
#### 1. 创建springboot-config-repo项目
当配置服务器创建好后，将可以用http 请求访问到这些配置文件的内容

http请求地址和资源文件映射如下:
```
/{application}/{profile}[/{label}]  
/{application}-{profile}.yml
/{label}/{application}-{profile}.yml
/{application}-{profile}.properties
/{label}/{application}-{profile}.properties
```
- {application} 对应着服务端中 `application.* `和 客户端中`bootstrap.*` 文件中 对应的“spring.application.name”;
- {profile} 则对应客户端中`bootstrap.*` 文件中 对应的“spring.active”  
- {label} 则是一个服务器端功能标签“版本”的（即git 的版本，默认master)

在这里，我创建 三个文件 
```
application.properties
config-client-dev.properties
config-server-dev.properties
```
分别在对应文件 中添加内容为 
```
foo=application
foo=config-client
foo=config-server
```
将此文件提交到本地的git 中
```
echo foo=application > application.properties
echo foo=config-client > config-client-dev.properties
echo foo=config-server > config-server-dev.properties
git init
git add .
git commit -m "config init"
```
为后面演示做示例

**Spring cloud使用git或svn存放配置文件，默认情况下使用git.因此你需要安装git私服或者直接使用互联网上的github或者git.oschina，这里推荐使用git.oschina。**

#### 2. 创建Spring cloud 配置服务器
该服务器将配置文件转换为 rest接口服务。
即：读取远程配置文件，转换为rest接口服务。
-  创建一个空的maven web项目，如 config-server

- 添加一个类
```
package cloud.config.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@EnableConfigServer
public class ConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigServerApplication.class, args);
    }
}
```

@EnableConfigServer激活该应用为配置文件服务器.

- 修改配置文件 application.properties
```
server.port=8888
spring.profiles.active=dev
spring.cloud.config.server.git.uri=file:///D:/workspace_eclipse_vs_git2/springboot-config-repo/
spring.cloud.config.server.git.searchPaths=springboot-config-repo
spring.cloud.config.label=master
spring.application.name=config-server
#localhost:8888/env
#localhost:8888/config-server/dev
#localhost:8888/config-server/default
```
- 其中:   
    - server.port是配置当前web应用绑定8888端口，   
    - spring.cloud.config.server.git.uri：配置git仓库地址
    - spring.cloud.config.server.git.searchPaths：配置仓库路径
    - spring.cloud.config.label：配置仓库的分支
    - spring.cloud.config.server.git.username：访问git仓库的用户名
    - spring.cloud.config.server.git.password：访问git仓库的用户密码  
如果Git仓库为公开仓库，可以不填写用户名和密码，如果是私有仓库需要填写

> 看我的配置文件，我想 聪明的你一定看出来了，uri 我采用的是本地的git，searchPaths 就是刚才创建的配置文件目录了  
> 当然正式发布是建议 采用远程的git  
> 还有项目名称指定为了 config-server，这个是为了与配置文件命名保持一致的，类似于项目的别名

- 修改pom 文件，加入依赖
```
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-config-server</artifactId>
</dependency>
```
> 这个架包，是配置文件服务器的关键哦。

- 启动服务，配置服务器即搭建完成。
    - 访问 http://localhost:8888/config-server/dev
    - 访问 http://localhost:8888/config-server/default
    - http://localhost:8888/config-client/dev
得到如下信息，证明配置服务中心可以从远程程序获取配置信息。

```
{"name":"config-server","profiles":["dev"],"label":"master","version":"47fba534b1151b0d5e8e98d24c1599729943afad","propertySources":[{"name":"file:///D:/workspace_eclipse_vs_git2/springboot-config-repo/config-server-dev.properties","source":{"foo":"config-server"}},{"name":"file:///D:/workspace_eclipse_vs_git2/springboot-config-repo/application.properties","source":{"foo":"application"}}]}
```
至此，服务器便发布成功了

#### 3. 创建一个客户端使用该远程配置
- 创建一个Maven 项目
- 创建一个启动类
- 创建一个Controller，来使用配置文件中的字段
```
package com.vc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController
{
	@Value("${foo}")
	String bar;

	@RequestMapping("/hello")
	String hello()
	{
		return "Hello " + bar + "!";
	}

}

```

- 在pom.xml 引入依赖
```
<dependency>
	<groupId>org.springframework.cloud</groupId>
	<artifactId>spring-cloud-starter-config</artifactId>
</dependency>
```
- 配置文件 bootstrap.*
```
server:
  port: 8881
spring:
  application:
    name: config-client
  cloud:
    config:
      profile: dev
      label: master
      uri: http://localhost:8888

```
> 注意这里是bootstrap.yml而不是appliction.yml,因为bootstrap.yml会在应用启动之前读取,而spring.cloud.config.uri会影响应用启动

- 启动项目，访问 http://localhost:8881/hello 即可看到结果。结果正确与否，自己看吧。

> 如果项目没有成功，那么记得重启几次，项目的启动顺序也是有影响的，尤其的配置文件修改了需要生效。

#### 参考
1. [使用spring cloud实现分布式配置管理](http://www.cnblogs.com/skyblog/p/5129603.html)

2. [spring_cloud_config](http://projects.spring.io/spring-cloud/spring-cloud.html#_spring_cloud_config)
3. [ 史上最简单的SpringCloud教程 | 第六篇: 分布式配置中心(Spring Cloud Config)](http://blog.csdn.net/forezp/article/details/70037291)
4. [Spring Cloud实战(一)-Spring Cloud Config Server](https://segmentfault.com/a/1190000006138698)
