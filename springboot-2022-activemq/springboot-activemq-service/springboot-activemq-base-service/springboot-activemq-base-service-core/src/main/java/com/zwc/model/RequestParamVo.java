package com.zwc.model;

import lombok.Data;

/**
 * This is description
 *
 * @author panie
 * @since 2022/10/9 11:50
 */
@Data
public class RequestParamVo {
    private String data;
    private String topic;
}
