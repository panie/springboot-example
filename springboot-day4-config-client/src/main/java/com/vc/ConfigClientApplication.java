package com.vc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ConfigClientApplication
{
	@Value("${foo}")
	String bar;

	@RequestMapping("/hello")
	String hello()
	{
		return "Hello " + bar + "!";
	}
	
	@ResponseBody
	@RequestMapping(value = "/")
	String home()
	{
		return "Hello World!";
	}
	public static void main(String[] args) throws Exception
	{
		SpringApplication.run(ConfigClientApplication.class, args);
	}

}
