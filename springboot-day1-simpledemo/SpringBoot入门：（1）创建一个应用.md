> #### 前言
> 每个人都有属于自己的理解方式，别人写的再好，终究只有自己的笔记，才是自己的习惯方式。故这里我将按照我的喜好，将我学习SpringBoot的学习之路记录下来。  
> 前人栽树，后人乘凉。还之以苗，以期来者。

## 任务
熟悉 SpringBoot，并使用SpringBoot创建一个应用

## 环境
* Eclipse 4.5
* JDK 1.8
* windows
* maven 3.x

## 创建一个maven项目
* 可以直接从 spring官网上下载样例，导入项目中
* 手动创建

### 项目内容详情
* pom.xml
```
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.vc</groupId>
	<artifactId>Test</artifactId>
	<packaging>war</packaging>
	<version>0.0.1-SNAPSHOT</version>
	<name>Test Maven Webapp</name>
	<url>http://maven.apache.org</url>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.4.5.RELEASE</version>
		<relativePath></relativePath>
	</parent>
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<spring.boot.admin.version>1.4.6</spring.boot.admin.version>
		<java.version>1.8</java.version>
	</properties>
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>
	</dependencies>
	<build>
		<finalName>Test</finalName>
		<plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
	</build>
</project>

```

* 一个类
```
package com.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@SpringBootApplication
public class TestController  {
 
    @ResponseBody
    @RequestMapping(value = "/")
    String home() {   
        return "Hello World!";
    }
 
    public static void main(String[] args) throws Exception {
        SpringApplication.run(TestController.class, args);
    }
 
}


```
　@SpringBootApplication相当于@Configuration、@EnableAutoConfiguration和  @ComponentScan，你也可以同时使用这3个注解。其中@Configuration、@ComponentScan是spring框架的语法，在spring 3.x就有了，用于代码方式创建配置信息和扫描包。@EnableAutoConfiguration是spring boot语法，表示将使用自动配置。你如果下载了spring boot源码，就会看到spring boot实现了很多starter应用，这些starter就是一些配置信息（有点类似于docker，一组环境一种应用的概念），spring boot看到引入的starter包，就可以计算如果自动配置你的应用。
　
* 运行项目
    * 直接 运行TestController类，Run Java Application 就可以看到项目启动了。
    * 或者 点击 F11
    * 项目启动后 在浏览器输入http://localhost:8080/，你会看到helloworld字样，这是一个web应用，使用了嵌入式的tomcat。
    
    
## 修改配置文件（定制内嵌Tomcat）
在 src/main/resources/application.properties 中配置Tomcat信息。


```
server.port=8089
server.tomcat.max-threads=1000
server.tomcat.uri-encoding = UTF-8
```

`server.port` 设置内嵌Tomcat的端口

`server.tomcat.max-threads` 设置内嵌的 Tomcat 服务器的最大线程数

`server.tomcat.uri-encoding` 设置内嵌Tomcat的编码

## 引入 监控
Spring boot提供的一些开箱即用的应用非常容易使用，比如监控，你只需要在pom文件中引入：

```
<!-- 可以实时更新、监控 start -->
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

引入之后，spring boot是默认开启监控的，运行应用你可以在浏览器中输入：
`
         http://localhost:8080/health
`
　　就可以看到默认的监控信息了：
```
 　　{"status":"UP","diskSpace":{"status":"UP","total":161067397120,"free":91618398208,"threshold":10485760}}
```
　　信息包括程序执行状态以及基本的磁盘信息。