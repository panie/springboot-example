# springboot-example
将平时学习使用springboot的例子保留起来。
前人栽树，后人乘凉。还之以苗，以期来者。

- sprintboot-day1 描述了一个简单的springboot 例子，用于认识 springboot，了解其启动

- springboot-day2 描述了一个 springboot 使用 jpa 来访问 mysql数据库的例子

- spirngboot-day3 描述了采用 springboot 集中管理配置项
- springboot-day4 采用 Eureka 对 配置管理优化。并且 jpa 的配置也来源于配置管理中心。
