package com.vc.im.model.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import com.vc.im.dao.ThirdPartyUserDao;
import com.vc.im.model.entity.ThirdPartyUser;
import com.vc.im.model.service.IThirdPartyUserService;


@Order
@Service
public class ThirdPartyUserService implements IThirdPartyUserService
{
	@Autowired
	private ThirdPartyUserDao userDao;

	public List<ThirdPartyUser> findAll()
	{
		return userDao.findAll();
	}

	@Override
	public ThirdPartyUser save(ThirdPartyUser user)
	{
		return userDao.save(user);
	}
	
	

}
