package com.vc.im.model.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.druid.pool.DruidDataSource;
import com.vc.commons.config.db.DruidDataSourceConfig;

@RestController
@RequestMapping("/config")
public class ConfigController
{
	@Autowired
	private DruidDataSourceConfig druidDBConfig;

	/**
	 * 
	 * @return
	 */
	@RequestMapping("/")
	@ResponseBody
	public DruidDataSource getList()
	{
		DruidDataSource dataSource = new DruidDataSource();
		BeanUtils.copyProperties(druidDBConfig, dataSource);
		System.out.println("dataSource=" + dataSource);
		return dataSource;
	}

}
