package com.vc.commons.config.db;

import javax.sql.DataSource;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.alibaba.druid.pool.DruidDataSource;

/**
 * 默认的数据源
 *
 */
@Component
public class DefaultDataSource {
	
	@Autowired
	private DruidDataSourceConfig druidDBConfig;
	
	/**
	 * 声明其为Bean实例 在同样的DataSource中，首先使用被标注的DataSource
	 * 
	 * @return
	 */
	@Bean
	@Primary
	public DataSource dataSource() {
		DruidDataSource datasource = new DruidDataSource();
		BeanUtils.copyProperties(druidDBConfig, datasource);
		return datasource;
	}
}
