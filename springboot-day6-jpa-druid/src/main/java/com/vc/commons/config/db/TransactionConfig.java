package com.vc.commons.config.db;

import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.ImportResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 事务切面
 * @author Cris
 *
 */
@Component
@EnableTransactionManagement(mode=AdviceMode.ASPECTJ,proxyTargetClass=true)
@ImportResource(locations={"classpath:spring.xml"})
public class TransactionConfig {

}
