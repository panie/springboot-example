package com.vc.commons.api.service.headers;

public interface IBaseClientHeaders {
	
	//远程服务调用
	public static final String HEADER = "Client-Request=REMOTE_SERVICE_INVOK";
	public static final String CLIENT_HEADER_KEY = "Client-Request";
	public static final String CLIENT_HEADER_VALUE = "REMOTE_SERVICE_INVOK";
	
}
