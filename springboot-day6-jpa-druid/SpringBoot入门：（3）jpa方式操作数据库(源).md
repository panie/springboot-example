> __前言__   
> 此项目在 Eureka配置中心 和 Jpa 方式操作数据库 两个实例完成的基础上 修改完善。   
> _并不是每一个项目都需要从头开始,若是我们已经站在了巨人的肩膀上，可以站得高看得远，又何必纠结不是自己长的高呢？_


### 初始化项目
 一般上可以在spring官网上初始化SpringBoot项目 [https://start.spring.io/ ](https://start.spring.io/ )   
 这里将之前做的jpa（day4） 的项目复制一遍，在此基础上修改。
 
#### 实现理论
 采用druid 作为数据源
 
#### druid
Druid介绍
     Druid是一个JDBC组件，它包括三部分： 
- DruidDriver 代理Driver，能够提供基于Filter－Chain模式的插件体系。
- DruidDataSource 高效可管理的数据库连接池。 
-SQLParser 
   
Druid可以做什么？  
- 可以监控数据库访问性能，Druid内置提供了一个功能强大的StatFilter插件，能够详细统计SQL的执行性能，这对于线上分析数据库访问性能有帮助。
- 替换DBCP和C3P0。Druid提供了一个高效、功能强大、可扩展性好的数据库连接池。
- 数据库密码加密。直接把数据库密码写在配置文件中，这是不好的行为，容易导致安全问题。DruidDruiver和DruidDataSource都支持PasswordCallback。  
- SQL执行日志，Druid提供了不同的LogFilter，能够支持Common-Logging、Log4j和JdkLog，你可以按需要选择相应的LogFilter，监控你应用的数据库访问情况。 
- 扩展JDBC，如果你要对JDBC层有编程的需求，可以通过Druid提供的Filter-Chain机制，很方便编写JDBC层的扩展插件。   
  
项目地址： https://github.com/alibaba/druid
     
#### 在原有JPA项目pom.xml中添加依赖:
```
<dependency>  
    <groupId>com.alibaba</groupId>  
    <artifactId>druid</artifactId>  
</dependency>  
```

#### 在appilication.properties中添加：
```
server.port=8040
spring.application.name=jpa-druid-client
spring.cloud.config.label=master
spring.cloud.config.profile=dev
#spring.cloud.config.uri= http://localhost:8888/

eureka.client.serviceUrl.defaultZone=http://localhost:8010/eureka/
spring.cloud.config.discovery.enabled=true
spring.cloud.config.discovery.serviceId=config-server
    
```

#### 在原有的项目springboot-config-repo 中新建一个文件，文件名与 `spring.application.name` 有关联
```
spring.datasource.type=com.alibaba.druid.pool.DruidDataSource
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
spring.datasource.url= jdbc:mysql://localhost:3306/vcdb?useUnicode=true&characterEncoding=utf8&characterSetResults=utf8
spring.datasource.username= root
spring.datasource.password= 123456
spring.datasource.initialSize=5
spring.datasource.minIdle=5
spring.datasource.maxActive=20
spring.datasource.maxWait=60000
#指定空闲连接检查、废弃连接清理、空闲连接池大小调整之间的操作时间间隔
spring.datasource.timeBetweenEvictionRunsMillis=60000
spring.datasource.minEvictableIdleTimeMillis=300000
spring.datasource.validationQuery=SELECT 1 FROM DUAL
#当连接空闲时，是否执行连接测试.
spring.datasource.testWhileIdle=true
#当从连接池借用连接时，是否测试该连接.
spring.datasource.testOnBorrow=false  
spring.datasource.testOnReturn=false
spring.datasource.poolPreparedStatements=true
spring.datasource.maxPoolPreparedStatementPerConnectionSize=20
spring.datasource.filters=stat,wall,log4j
spring.datasource.connectionProperties=druid.stat.mergeSql=true;druid.stat.slowSqlMillis=5000
spring.datasource.useGlobalDataSourceStat=true

spring.jpa.database=mysql 

#指定DDL mode (none, validate, update, create, create-drop). 当使用内嵌数据库时，默认是create-drop，否则为none.
spring.jpa.hibernate.ddl-auto=update

#是否开启sql的log，默认为: false
spring.jpa.hibernate.show-sql=true
```
> 注：ddl_auto: create 代表在数据库创建表，update 代表更新，首次启动需要create ,如果你想通过hibernate 注解的方式创建数据库的表的话，之后需要改为 update.


#### 创建几个实体
DruidDataSourceConfig、DefaultDataSource 可以获取数据源配置详情
DruidConfiguration 可以查询详细统计SQL的执行情况   

_类详情见项目_

#### 启动
启动顺序为 eureka-server / config-server / springboot-day5-jpa-druid

运行界面以及介绍
    访问地址： http://localhost:8080/druid/index.html
    
    登录密码 查看DruidConfiguration类中细节

参考:[ Spring Boot 使用 Druid 和监控配置](http://blog.csdn.net/catoop/article/details/50925337)  
[Spring Boot下Druid连接池的使用配置分析](http://blog.csdn.net/blueheart20/article/details/52384032)