# 此项目为 java 连接 kafka 的示例
## 项目结构为 
```
springboot-kafka-commons 为基础常量包， 用于存放公共包与常量、配置
springboot-kafka-service 为实现业务的包
    springboot-kafka-base-service 为生产者示例
    springboot-kafka-user-service 为消费者示例
```    
    
## 运行方式：
```
    1） 打开idea， File -> New -> Project From Existing Sources ， 选择 springboot-kafka-commons。 下一步直到 导入项目成功
    2） 在前面导入成功的项目页中，  File -> New -> Module From Existing Sources ， 选择 springboot-kafka-service。下一步直到 导入项目成功
    3） 正确配置 springboot-kafka-commons 中的配置文件
    4） 运行 springboot-kafka-service 里面的 带有 @SpringBootApplication 注解的类， 即可成功运行项目
```

## 本项目的实现思路：
    采用多模块的结构， 将公共常量提取出来作为不同项目的公用部分。
    
    
