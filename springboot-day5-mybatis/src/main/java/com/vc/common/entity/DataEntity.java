package com.vc.common.entity;

import java.util.Date;

/**
 * 数据Entity类
 */
public abstract class DataEntity<T> extends BaseEntity<T>
{

	private static final long serialVersionUID = 1L;

	private String remarks; // 备注信息
	private Date createTime; // 创建时间

	private Date updateTime; // 修改时间

	public DataEntity()
	{
		super();
	}

	public String getRemarks()
	{
		return remarks;
	}

	public void setRemarks(String remarks)
	{
		this.remarks = remarks;
	}

	public Date getCreateTime()
	{
		return createTime;
	}

	public void setCreateTime(Date createTime)
	{
		this.createTime = createTime;
	}

	public Date getUpdateTime()
	{
		return updateTime;
	}

	public void setUpdateTime(Date updateTime)
	{
		this.updateTime = updateTime;
	}

	/**
	 * 插入之前执行方法，需要手动调用
	 */
	@Override
	public void preInsert()
	{
		this.updateTime = new Date();
		this.createTime = this.updateTime;
	}

	/**
	 * 更新之前执行方法，需要手动调用
	 */
	@Override
	public void preUpdate()
	{
		this.updateTime = new Date();
	}

}
