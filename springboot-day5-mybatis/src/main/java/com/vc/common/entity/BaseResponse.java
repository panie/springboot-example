package com.vc.common.entity;

public class BaseResponse
{
    
    private String statusCode;
    
    private String msg;
    
    private Object data;
    
    public BaseResponse()
    {
        super();
        this.statusCode = "";
    }
    
    public String getStatusCode()
    {
        return statusCode;
    }
    
    public void setStatusCode(String statusCode)
    {
        this.statusCode = statusCode;
    }
    
    public String getMsg()
    {
        return msg;
    }
    
    public void setMsg(String msg)
    {
        this.msg = msg;
    }
    
    public Object getData()
    {
        return data;
    }
    
    public void setData(Object data)
    {
        this.data = data;
    }
    
}
