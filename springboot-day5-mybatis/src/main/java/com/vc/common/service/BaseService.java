package com.vc.common.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.vc.common.dao.BaseDao;
import com.vc.common.entity.DataEntity;

@Transactional(readOnly = true)
public abstract class BaseService<D extends BaseDao<T>, T extends DataEntity>
{

	/**
	 * 日志对象
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 持久层对象
	 */
	@Autowired
	protected D dao;

	/**
	 * 获取单条数据
	 * 
	 * @param entity
	 * @return
	 */
	public T get(T entity)
	{
		return dao.get(entity);
	}

	/**
	 * 用于条件查询
	 * 
	 * @param entity
	 * @return
	 */
	public List<T> searchList(String searchValue)
	{
		return dao.searchList(searchValue);
	}

	/**
	 * 查询列表数据
	 * 
	 * @param entity
	 * @return
	 */
	public List<T> getList(T entity)
	{
		return dao.getList(entity);
	}

	public List<T> getAllList(T entity)
	{
		return dao.getAllList(entity);
	}

	/**
	 * 保存数据（插入或更新）
	 * 
	 * @param entity
	 */
	@Transactional(readOnly = false)
	public void save(T entity)
	{
		if (entity.getIsNewRecord())
		{
			entity.preInsert();
			dao.insertSelective(entity);
		} else
		{
			entity.preUpdate();
			dao.updateSelective(entity);
		}
	}

	/**
	 * 删除数据
	 * 
	 * @param entity
	 */
	@Transactional(readOnly = false)
	public void delete(T entity)
	{
		dao.delete(entity);
	}

	/**
	 * 批量删除
	 * 
	 * @param entity
	 * @return
	 */
	@Transactional(readOnly = false)
	public int batchDelete(List<T> list)
	{
		return dao.batchDelete(list);
	}

	/**
	 * 批量插入
	 * 
	 * @param entity
	 * @return
	 */
	@Transactional(readOnly = false)
	public int batchInsert(List<T> list)
	{
		return dao.batchInsert(list);
	}

	/**
	 * 批量修改
	 * 
	 * @param list
	 * @return
	 */
	@Transactional(readOnly = false)
	public int batchUpdate(List<T> list)
	{
		return dao.batchUpdate(list);
	}

	/**
	 * 获取id数组，批量查询数据
	 * 
	 * @param entity
	 * @return
	 */
	public List<T> selectBatchIds(List<T> list)
	{
		return dao.selectBatchIds(list);
	}
}
