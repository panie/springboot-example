package com.vc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@MapperScan(basePackages = "com.vc")
@EnableConfigurationProperties
public class GroupVcImServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GroupVcImServiceApplication.class, args);
	}
}
