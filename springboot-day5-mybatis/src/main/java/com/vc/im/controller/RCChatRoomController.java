package com.vc.im.controller;

import java.io.Reader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vc.im.entity.RongCloudUser;

import io.rong.RongCloud;
import io.rong.models.ChatRoomInfo;
import io.rong.models.ChatroomQueryResult;
import io.rong.models.ChatroomUserQueryResult;
import io.rong.models.CodeSuccessResult;
import io.rong.models.GroupUserQueryResult;
import io.rong.models.ListBlockChatroomUserResult;
import io.rong.models.ListGagChatroomUserResult;
import io.rong.models.ListGagGroupUserResult;

@RestController
@RequestMapping("/im")
public class RCChatRoomController
{
    
    @Autowired
    private RongCloudUser rongCloudUser;
    
    
    /**
     * 添加敏感词方法（设置敏感词后，App 中用户不会收到含有敏感词的消息内容，默认最多设置 50 个敏感词。）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("wordfilterDelete1")
    @ResponseBody
    public Object wordfilterDelete()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        // 移除敏感词方法（从敏感词列表中，移除某一敏感词。）
        CodeSuccessResult wordfilterDeleteResult = rongCloud.wordfilter.delete("money");
        System.out.println("delete:  " + wordfilterDeleteResult.toString());
        
        return wordfilterDeleteResult;
    }
    
    
    /**
     * 本地调用测试
     * 
     * @param args
     * @throws Exception
     */
    public static void main(String[] args)
        throws Exception
    {
        String appKey = "pvxdm17jpioar";// 替换成您的appkey
        String appSecret = "LchfaqZd3a6Gf";// 替换成匹配上面key的secret
        
        Reader reader = null;
        RongCloud rongCloud = RongCloud.getInstance(appKey, appSecret);
        
        System.out.println("************************Group********************");
        // 刷新群组信息方法
        CodeSuccessResult groupRefreshResult = rongCloud.group.refresh("groupId1", "newGroupName");
        System.out.println("refresh:  " + groupRefreshResult.toString());
        
        // 将用户加入指定群组，用户将可以收到该群的消息，同一用户最多可加入 500 个群，每个群最大至 3000 人。
        String[] groupJoinUserId = {"userId2", "userid3", "userId4"};
        CodeSuccessResult groupJoinResult = rongCloud.group.join(groupJoinUserId, "groupId1", "TestGroup");
        System.out.println("join:  " + groupJoinResult.toString());
        
        // 查询群成员方法
        GroupUserQueryResult groupQueryUserResult = rongCloud.group.queryUser("groupId1");
        System.out.println("queryUser:  " + groupQueryUserResult.toString());
        
        // 退出群组方法（将用户从群中移除，不再接收该群组的消息.）
        String[] groupQuitUserId = {"userId2", "userid3", "userId4"};
        CodeSuccessResult groupQuitResult = rongCloud.group.quit(groupQuitUserId, "TestGroup");
        System.out.println("quit:  " + groupQuitResult.toString());
        
        // 添加禁言群成员方法（在 App 中如果不想让某一用户在群中发言时，可将此用户在群组中禁言，被禁言用户可以接收查看群组中用户聊天信息，但不能发送消息。）
        CodeSuccessResult groupAddGagUserResult = rongCloud.group.addGagUser("userId1", "groupId1", "1");
        System.out.println("addGagUser:  " + groupAddGagUserResult.toString());
        
        // 查询被禁言群成员方法
        ListGagGroupUserResult groupLisGagUserResult = rongCloud.group.lisGagUser("groupId1");
        System.out.println("lisGagUser:  " + groupLisGagUserResult.toString());
        
        // 移除禁言群成员方法
        String[] groupRollBackGagUserUserId = {"userId2", "userid3", "userId4"};
        CodeSuccessResult groupRollBackGagUserResult =
            rongCloud.group.rollBackGagUser(groupRollBackGagUserUserId, "groupId1");
        System.out.println("rollBackGagUser:  " + groupRollBackGagUserResult.toString());
        
        // 解散群组方法。（将该群解散，所有用户都无法再接收该群的消息。）
        CodeSuccessResult groupDismissResult = rongCloud.group.dismiss("userId1", "groupId1");
        System.out.println("dismiss:  " + groupDismissResult.toString());
        
        System.out.println("************************Chatroom********************");
        // 创建聊天室方法
        ChatRoomInfo[] chatroomCreateChatRoomInfo = {new ChatRoomInfo("chatroomId1", "chatroomName1"),
            new ChatRoomInfo("chatroomId2", "chatroomName2"), new ChatRoomInfo("chatroomId3", "chatroomName3")};
        CodeSuccessResult chatroomCreateResult = rongCloud.chatroom.create(chatroomCreateChatRoomInfo);
        System.out.println("create:  " + chatroomCreateResult.toString());
        
        // 加入聊天室方法
        String[] chatroomJoinUserId = {"userId2", "userid3", "userId4"};
        CodeSuccessResult chatroomJoinResult = rongCloud.chatroom.join(chatroomJoinUserId, "chatroomId1");
        System.out.println("join:  " + chatroomJoinResult.toString());
        
        // 查询聊天室信息方法
        String[] chatroomQueryChatroomId = {"chatroomId1", "chatroomId2", "chatroomId3"};
        ChatroomQueryResult chatroomQueryResult = rongCloud.chatroom.query(chatroomQueryChatroomId);
        System.out.println("query:  " + chatroomQueryResult.toString());
        
        // 查询聊天室内用户方法
        ChatroomUserQueryResult chatroomQueryUserResult = rongCloud.chatroom.queryUser("chatroomId1", "500", "2");
        System.out.println("queryUser:  " + chatroomQueryUserResult.toString());
        
        // 聊天室消息停止分发方法（可实现控制对聊天室中消息是否进行分发，停止分发后聊天室中用户发送的消息，融云服务端不会再将消息发送给聊天室中其他用户。）
        CodeSuccessResult chatroomStopDistributionMessageResult =
            rongCloud.chatroom.stopDistributionMessage("chatroomId1");
        System.out.println("stopDistributionMessage:  " + chatroomStopDistributionMessageResult.toString());
        
        // 聊天室消息恢复分发方法
        CodeSuccessResult chatroomResumeDistributionMessageResult =
            rongCloud.chatroom.resumeDistributionMessage("chatroomId1");
        System.out.println("resumeDistributionMessage:  " + chatroomResumeDistributionMessageResult.toString());
        
        // 添加禁言聊天室成员方法（在 App 中如果不想让某一用户在聊天室中发言时，可将此用户在聊天室中禁言，被禁言用户可以接收查看聊天室中用户聊天信息，但不能发送消息.）
        CodeSuccessResult chatroomAddGagUserResult = rongCloud.chatroom.addGagUser("userId1", "chatroomId1", "1");
        System.out.println("addGagUser:  " + chatroomAddGagUserResult.toString());
        
        // 查询被禁言聊天室成员方法
        ListGagChatroomUserResult chatroomListGagUserResult = rongCloud.chatroom.ListGagUser("chatroomId1");
        System.out.println("ListGagUser:  " + chatroomListGagUserResult.toString());
        
        // 移除禁言聊天室成员方法
        CodeSuccessResult chatroomRollbackGagUserResult = rongCloud.chatroom.rollbackGagUser("userId1", "chatroomId1");
        System.out.println("rollbackGagUser:  " + chatroomRollbackGagUserResult.toString());
        
        // 添加封禁聊天室成员方法
        CodeSuccessResult chatroomAddBlockUserResult = rongCloud.chatroom.addBlockUser("userId1", "chatroomId1", "1");
        System.out.println("addBlockUser:  " + chatroomAddBlockUserResult.toString());
        
        // 查询被封禁聊天室成员方法
        ListBlockChatroomUserResult chatroomGetListBlockUserResult = rongCloud.chatroom.getListBlockUser("chatroomId1");
        System.out.println("getListBlockUser:  " + chatroomGetListBlockUserResult.toString());
        
        // 移除封禁聊天室成员方法
        CodeSuccessResult chatroomRollbackBlockUserResult =
            rongCloud.chatroom.rollbackBlockUser("userId1", "chatroomId1");
        System.out.println("rollbackBlockUser:  " + chatroomRollbackBlockUserResult.toString());
        
        // 添加聊天室消息优先级方法
        String[] chatroomAddPriorityObjectName = {"RC:VcMsg", "RC:ImgTextMsg", "RC:ImgMsg"};
        CodeSuccessResult chatroomAddPriorityResult = rongCloud.chatroom.addPriority(chatroomAddPriorityObjectName);
        System.out.println("addPriority:  " + chatroomAddPriorityResult.toString());
        
        // 销毁聊天室方法
        String[] chatroomDestroyChatroomId = {"chatroomId", "chatroomId1", "chatroomId2"};
        CodeSuccessResult chatroomDestroyResult = rongCloud.chatroom.destroy(chatroomDestroyChatroomId);
        System.out.println("destroy:  " + chatroomDestroyResult.toString());
        
        // 添加聊天室白名单成员方法
        String[] chatroomAddWhiteListUserUserId = {"userId1", "userId2", "userId3", "userId4", "userId5"};
        CodeSuccessResult chatroomAddWhiteListUserResult =
            rongCloud.chatroom.addWhiteListUser("chatroomId", chatroomAddWhiteListUserUserId);
        System.out.println("addWhiteListUser:  " + chatroomAddWhiteListUserResult.toString());
    }
    
}
