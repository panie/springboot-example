package com.vc.im.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vc.im.entity.RongCloudUser;

import io.rong.RongCloud;
import io.rong.models.SMSImageCodeResult;
import io.rong.models.SMSSendCodeResult;
import io.rong.models.SMSVerifyCodeResult;

@RestController
@RequestMapping("/im")
public class RCCodeController
{
    @Autowired
    private RongCloudUser rongCloudUser;
    
    /**
     * 添加敏感词方法（设置敏感词后，App 中用户不会收到含有敏感词的消息内容，默认最多设置 50 个敏感词。）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("verifyCode")
    @ResponseBody
    public Object verifyCode()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        // 验证码验证方法
        SMSVerifyCodeResult sMSVerifyCodeResult = rongCloud.sms.verifyCode("2312312", "2312312");
        System.out.println("verifyCode:  " + sMSVerifyCodeResult.toString());
        
        return sMSVerifyCodeResult;
    }
    
    /**
     * 发送短信验证码方法
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("sMSSendCode")
    @ResponseBody
    public Object groupCreate()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        // 发送短信验证码方法。
        SMSSendCodeResult sMSSendCodeResult =
            rongCloud.sms.sendCode("13500000000", "dsfdsfd", "86", "1408706337", "1408706337");
        System.out.println("sendCode:  " + sMSSendCodeResult.toString());
        return sMSSendCodeResult;
    }
    
    /**
     * 获取图片验证码方法
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("sMSGetImageCode")
    @ResponseBody
    public Object sMSGetImageCode()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        // 获取图片验证码方法
        SMSImageCodeResult sMSGetImageCodeResult = rongCloud.sms.getImageCode(rongCloudUser.getAppKey());
        System.out.println("getImageCode:  " + sMSGetImageCodeResult.toString());
        return sMSGetImageCodeResult;
    }
        
}
