package com.vc.im.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vc.im.entity.RongCloudUser;
import com.vc.modules.entity.User;

import io.rong.RongCloud;
import io.rong.models.CheckOnlineResult;
import io.rong.models.CodeSuccessResult;
import io.rong.models.QueryBlacklistUserResult;
import io.rong.models.QueryBlockUserResult;
import io.rong.models.TokenResult;

@RestController
@RequestMapping("/im")
public class RCUserController
{
    
    @Autowired
    private RongCloudUser rongCloudUser;
    
    /**
     * 获取 Token
     * 
     * @param userId String 用户 Id，最大长度 64 字节。是用户在 App 中的唯一标识码（必传）
     * @param name String 用户名称，最大长度 128 字节。（必传）
     * @param portraitUri 用户头像 URI，最大长度 1024 字节。（必传）
     * @return
     * @throws Exception
     */
    @RequestMapping("getToken")
    @ResponseBody
    public TokenResult getToken(User entity)
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        TokenResult userGetTokenResult =
            rongCloud.user.getToken(entity.getId() + "", entity.getUsername(), entity.getFace());
        return userGetTokenResult;
    }
    
    /**
     * 刷新用户信息方法
     * 
     * @param userId String 用户 Id，最大长度 64 字节。是用户在 App 中的唯一标识码（必传）
     * @param name String 用户名称，最大长度 128 字节。（必传）
     * @param portraitUri 用户头像 URI，最大长度 1024 字节。（必传）
     * @return
     * @throws Exception
     */
    @RequestMapping("refresh")
    @ResponseBody
    public Object refresh(User entity)
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        // 刷新用户信息方法
        CodeSuccessResult userRefreshResult =
            rongCloud.user.refresh(entity.getId() + "", entity.getUsername(), entity.getFace());
        System.out.println("refresh:  " + userRefreshResult.toString());
        return userRefreshResult;
    }
    
    /**
     * // 检查用户在线状态 方法
     * 
     * @param userId String 用户 Id，最大长度 64 字节。是用户在 App 中的唯一标识码（必传）
     * @return
     * @throws Exception
     */
    @RequestMapping("checkOnline")
    @ResponseBody
    public Object checkOnline(User entity)
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        
        CheckOnlineResult userCheckOnlineResult = rongCloud.user.checkOnline(entity.getId() + "");
        System.out.println("checkOnline:  " + userCheckOnlineResult.toString());
        return userCheckOnlineResult;
    }
    
    /**
     * 封禁用户方法（每秒钟限 100 次）
     * 
     * @param userId String 用户 Id，最大长度 64 字节。是用户在 App 中的唯一标识码（必传）
     * @return
     * @throws Exception
     */
    @RequestMapping("block")
    @ResponseBody
    public Object block(User entity)
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        
        CodeSuccessResult userBlockResult = rongCloud.user.block(entity.getId() + "", 10);
        System.out.println("block:  " + userBlockResult.toString());
        return userBlockResult;
    }
    
    /**
     * 解除用户封禁方法（每秒钟限 100 次）
     * 
     * @param userId String 用户 Id，最大长度 64 字节。是用户在 App 中的唯一标识码（必传）
     * @return
     * @throws Exception
     */
    @RequestMapping("unBlock")
    @ResponseBody
    public Object unBlock(User entity)
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        
        CodeSuccessResult userUnBlockResult = rongCloud.user.unBlock(entity.getId() + "");
        System.out.println("unBlock:  " + userUnBlockResult.toString());
        return userUnBlockResult;
    }
    
    /**
     * 获取被封禁用户方法（每秒钟限 100 次）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("queryBlock")
    @ResponseBody
    public Object queryBlock()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        
        QueryBlockUserResult userQueryBlockResult = rongCloud.user.queryBlock();
        System.out.println("queryBlock:  " + userQueryBlockResult.toString());
        return userQueryBlockResult;
    }
    
    /**
     * 获取被封禁用户方法（每秒钟限 100 次）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("addBlacklist")
    @ResponseBody
    public Object addBlacklist(User user, User blackUser)
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        
        // 添加用户到黑名单方法（每秒钟限 100 次）
        CodeSuccessResult userAddBlacklistResult =
            rongCloud.user.addBlacklist("" + user.getId(), "" + blackUser.getId());
        System.out.println("addBlacklist:  " + userAddBlacklistResult.toString());
        return userAddBlacklistResult;
    }
    
    /**
     * 获取某用户的黑名单列表方法（每秒钟限 100 次）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("queryBlacklist")
    @ResponseBody
    public Object queryBlacklist(User user)
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        
        QueryBlacklistUserResult userQueryBlacklistResult = rongCloud.user.queryBlacklist("" + user.getId());
        System.out.println("queryBlacklist:  " + userQueryBlacklistResult.toString());
        return userQueryBlacklistResult;
    }
    
    /**
     * 获取某用户的黑名单列表方法（每秒钟限 100 次）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("removeBlacklist")
    @ResponseBody
    public Object removeBlacklist(User user, User blackUser)
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        
        // 从黑名单中移除用户方法（每秒钟限 100 次）
        CodeSuccessResult userRemoveBlacklistResult =
            rongCloud.user.removeBlacklist("" + user.getId(), "" + blackUser.getId());
        System.out.println("removeBlacklist:  " + userRemoveBlacklistResult.toString());
        
        return userRemoveBlacklistResult;
    }
    
}
