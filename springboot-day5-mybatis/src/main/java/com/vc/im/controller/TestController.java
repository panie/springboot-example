package com.vc.im.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vc.common.utils.JsonMapper;
import com.vc.im.entity.RongCloudUser;

@RestController
@RequestMapping("/config")
public class TestController
{
    @Autowired
    private RongCloudUser rongCloudUser;
    
    @RequestMapping(method = RequestMethod.GET, value = "/")
    @ResponseBody
    public Object hello()
    {
        System.out.println(JsonMapper.getInstance().toJson(rongCloudUser));
        return rongCloudUser;
    }
}
