package com.vc.im.controller;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vc.im.constant.Constant;
import com.vc.im.entity.RongCloudUser;
import com.vc.modules.entity.User;

import io.rong.RongCloud;
import io.rong.messages.TxtMessage;
import io.rong.messages.VoiceMessage;
import io.rong.models.CodeSuccessResult;
import io.rong.models.HistoryMessageResult;
import io.rong.models.ListWordfilterResult;
import io.rong.models.TemplateMessage;
import io.rong.util.GsonUtil;

@RestController
@RequestMapping("/im")
public class RCMessagesController
{
    
    @Autowired
    private RongCloudUser rongCloudUser;
    
    /**
     * 发送单聊消息方法（一个用户向另外一个用户发送消息，单条消息最大 128k。每分钟最多发送 6000 条信息，每次发送用户上限为 1000 人，如：一次发送 1000 人时，示为 1000 条消息。）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("publishPrivate")
    @ResponseBody
    public Object publishPrivate(User user, User blackUser)
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        
        // 发送单聊消息方法（一个用户向另外一个用户发送消息，单条消息最大 128k。每分钟最多发送 6000 条信息，每次发送用户上限为 1000 人，如：一次发送 1000 人时，示为 1000 条消息。）
        String[] messagePublishPrivateToUserId = {"userId2", "userid3", "userId4"};
        VoiceMessage messagePublishPrivateVoiceMessage = new VoiceMessage("hello", "helloExtra", 20L);
        CodeSuccessResult messagePublishPrivateResult = rongCloud.message.publishPrivate("userId1",
            messagePublishPrivateToUserId,
            messagePublishPrivateVoiceMessage,
            "thisisapush",
            "{\"pushData\":\"hello\"}",
            "4",
            0,
            0,
            0,
            0);
        System.out.println("publishPrivate:  " + messagePublishPrivateResult.toString());
        
        return messagePublishPrivateResult;
    }
    
    /**
     * 发送系统消息方法（一个用户向一个或多个用户发送系统消息，单条消息最大 128k，会话类型为 SYSTEM。 每秒钟最多发送 100 条消息，每次最多同时向 100 人发送，如：一次发送 100 人时，示为 100 条消息。）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("PublishSystem")
    @ResponseBody
    public Object PublishSystem()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        
        String[] messagePublishSystemToUserId = {"userId2", "userid3", "userId4"};
        TxtMessage messagePublishSystemTxtMessage = new TxtMessage("hello", "helloExtra");
        CodeSuccessResult messagePublishSystemResult = rongCloud.message.PublishSystem("userId1",
            messagePublishSystemToUserId,
            messagePublishSystemTxtMessage,
            "thisisapush",
            "{\"pushData\":\"hello\"}",
            0,
            0);
        System.out.println("PublishSystem:  " + messagePublishSystemResult.toString());
        return messagePublishSystemResult;
    }
    
    /**
     * 发送系统消息方法（一个用户向一个或多个用户发送系统消息，单条消息最大 128k，会话类型为 SYSTEM。 每秒钟最多发送 100 条消息，每次最多同时向 100 人发送，如：一次发送 100 人时，示为 100 条消息。）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("publishSystemTemplate")
    @ResponseBody
    public Object publishSystemTemplate()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        Reader reader = null;
        // 发送系统模板消息方法（一个用户向一个或多个用户发送系统消息，单条消息最大 128k，会话类型为 SYSTEM.每秒钟最多发送 100 条消息，每次最多同时向 100 人发送，如：一次发送 100 人时，示为 100
        // 条消息。）
        try
        {
            reader = new InputStreamReader(new FileInputStream(Constant.JSONFILE + "TemplateMessage.json"));
            TemplateMessage publishSystemTemplateTemplateMessage =
                (TemplateMessage)GsonUtil.fromJson(reader, TemplateMessage.class);
            CodeSuccessResult messagePublishSystemTemplateResult =
                rongCloud.message.publishSystemTemplate(publishSystemTemplateTemplateMessage);
            System.out.println("publishSystemTemplate:  " + messagePublishSystemTemplateResult.toString());
            
            return messagePublishSystemTemplateResult;
        }
        catch (Exception e)
        {
            // TODO: handle exception
            e.printStackTrace();
        }
        finally
        {
            if (null != reader)
            {
                reader.close();
            }
        }
        return "ERROR";
    }
    
    /**
     * 发送群组消息方法（以一个用户身份向群组发送消息，单条消息最大 128k.每秒钟最多发送 20 条消息，每次最多向 3 个群组发送，如：一次向 3 个群组发送消息，示为 3 条消息。） *
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("publishGroup")
    @ResponseBody
    public Object publishGroup()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        // 发送群组消息方法（以一个用户身份向群组发送消息，单条消息最大 128k.每秒钟最多发送 20 条消息，每次最多向 3 个群组发送，如：一次向 3 个群组发送消息，示为 3 条消息。）
        String[] messagePublishGroupToGroupId = {"groupId1", "groupId2", "groupId3"};
        TxtMessage messagePublishGroupTxtMessage = new TxtMessage("hello", "helloExtra");
        CodeSuccessResult messagePublishGroupResult = rongCloud.message.publishGroup("userId",
            messagePublishGroupToGroupId,
            messagePublishGroupTxtMessage,
            "thisisapush",
            "{\"pushData\":\"hello\"}",
            1,
            1,
            0);
        System.out.println("publishGroup:  " + messagePublishGroupResult.toString());
        
        return messagePublishGroupResult;
    }
    
    /**
     * 发送讨论组消息方法（以一个用户身份向讨论组发送消息，单条消息最大 128k，每秒钟最多发送 20 条消息.）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("publishDiscussion")
    @ResponseBody
    public Object publishDiscussion()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        // 发送讨论组消息方法（以一个用户身份向讨论组发送消息，单条消息最大 128k，每秒钟最多发送 20 条消息.）
        TxtMessage messagePublishDiscussionTxtMessage = new TxtMessage("hello", "helloExtra");
        CodeSuccessResult messagePublishDiscussionResult = rongCloud.message.publishDiscussion("userId1",
            "discussionId1",
            messagePublishDiscussionTxtMessage,
            "thisisapush",
            "{\"pushData\":\"hello\"}",
            1,
            1,
            0);
        System.out.println("publishDiscussion:  " + messagePublishDiscussionResult.toString());
        
        return messagePublishDiscussionResult;
    }
    
    /**
     * 发送聊天室消息方法（一个用户向聊天室发送消息，单条消息最大 128k。每秒钟限 100 次。）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("publishChatroom")
    @ResponseBody
    public Object publishChatroom()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        
        // 发送聊天室消息方法（一个用户向聊天室发送消息，单条消息最大 128k。每秒钟限 100 次。）
        String[] messagePublishChatroomToChatroomId = {"ChatroomId1", "ChatroomId2", "ChatroomId3"};
        TxtMessage messagePublishChatroomTxtMessage = new TxtMessage("hello", "helloExtra");
        CodeSuccessResult messagePublishChatroomResult = rongCloud.message.publishChatroom("userId1",
            messagePublishChatroomToChatroomId,
            messagePublishChatroomTxtMessage);
        System.out.println("publishChatroom:  " + messagePublishChatroomResult.toString());
        
        return messagePublishChatroomResult;
    }
    
    /**
     * 发送广播消息方法（发送消息给一个应用下的所有注册用户，如用户未在线会对满足条件（绑定手机终端）的用户发送 Push 信息，单条消息最大 128k，会话类型为 SYSTEM。每小时只能发送 1 次，每天最多发送 3 次。）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("broadcast")
    @ResponseBody
    public Object broadcast()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        
        // 发送广播消息方法（发送消息给一个应用下的所有注册用户，如用户未在线会对满足条件（绑定手机终端）的用户发送 Push 信息，单条消息最大 128k，会话类型为 SYSTEM。每小时只能发送 1 次，每天最多发送 3
        // 次。）
        TxtMessage messageBroadcastTxtMessage = new TxtMessage("hello", "helloExtra");
        CodeSuccessResult messageBroadcastResult = rongCloud.message.broadcast("userId1",
            messageBroadcastTxtMessage,
            "thisisapush",
            "{\"pushData\":\"hello\"}",
            "iOS");
        System.out.println("broadcast:  " + messageBroadcastResult.toString());
        
        return messageBroadcastResult;
    }
    
    /**
     * 消息历史记录下载地址获取 方法消息历史记录下载地址获取方法。获取 APP 内指定某天某小时内的所有会话消息记录的下载地址。 （目前支持二人会话、讨论组、群组、聊天室、客服、系统通知消息历史记录下载）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("getHistory")
    @ResponseBody
    public Object getHistory()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        // 消息历史记录下载地址获取 方法消息历史记录下载地址获取方法。获取 APP 内指定某天某小时内的所有会话消息记录的下载地址。（目前支持二人会话、讨论组、群组、聊天室、客服、系统通知消息历史记录下载）
        HistoryMessageResult messageGetHistoryResult = rongCloud.message.getHistory("2014010101");
        System.out.println("getHistory:  " + messageGetHistoryResult.toString());
        
        return messageGetHistoryResult;
    }
    
    /**
     * 消息历史记录删除方法（删除 APP 内指定某天某小时内的所有会话消息记录。调用该接口返回成功后，date参数指定的某小时的消息记录文件将在随后的5-10分钟内被永久删除。） *
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("deleteMessage")
    @ResponseBody
    public Object deleteMessage()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        // 消息历史记录删除方法（删除 APP 内指定某天某小时内的所有会话消息记录。调用该接口返回成功后，date参数指定的某小时的消息记录文件将在随后的5-10分钟内被永久删除。）
        CodeSuccessResult messageDeleteMessageResult = rongCloud.message.deleteMessage("2014010101");
        System.out.println("deleteMessage:  " + messageDeleteMessageResult.toString());
        return messageDeleteMessageResult;
    }
    
    /**
     * 添加敏感词方法（设置敏感词后，App 中用户不会收到含有敏感词的消息内容，默认最多设置 50 个敏感词。）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("wordfilter")
    @ResponseBody
    public Object wordfilter(String wordfilter)
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        // 添加敏感词方法（设置敏感词后，App 中用户不会收到含有敏感词的消息内容，默认最多设置 50 个敏感词。）
        CodeSuccessResult wordfilterAddResult = rongCloud.wordfilter.add(wordfilter);
        System.out.println("add:  " + wordfilterAddResult.toString());
        return wordfilterAddResult;
    }
    
    /**
     * 添加敏感词方法（设置敏感词后，App 中用户不会收到含有敏感词的消息内容，默认最多设置 50 个敏感词。）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("wordfilterGetList")
    @ResponseBody
    public Object wordfilterGetList()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        // 查询敏感词列表方法
        ListWordfilterResult wordfilterGetListResult = rongCloud.wordfilter.getList();
        System.out.println("getList:  " + wordfilterGetListResult.toString());
        return wordfilterGetListResult;
    }
    
    /**
     * 添加敏感词方法（设置敏感词后，App 中用户不会收到含有敏感词的消息内容，默认最多设置 50 个敏感词。）
     * 
     * @return
     * @throws Exception
     */
    @RequestMapping("wordfilterDelete")
    @ResponseBody
    public Object wordfilterDelete()
        throws Exception
    {
        RongCloud rongCloud = RongCloud.getInstance(rongCloudUser);
        // 移除敏感词方法（从敏感词列表中，移除某一敏感词。）
        CodeSuccessResult wordfilterDeleteResult = rongCloud.wordfilter.delete("money");
        System.out.println("delete:  " + wordfilterDeleteResult.toString());
        
        return wordfilterDeleteResult;
    }
    
    
}
