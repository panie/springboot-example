package com.vc.im.constant;

import io.rong.Example;

public interface Constant
{
    String DEF_FACE = "http://www.rongcloud.cn/images/logo.png";
    
    String JSONFILE = Example.class.getClassLoader().getResource("jsonsource").getPath()+"/";
    
    String UTF8 = "UTF-8";
}
