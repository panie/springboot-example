package com.vc.im.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component  
@ConfigurationProperties(locations = "classpath:config.properties", prefix = "rongcloud")
public class RongCloudUser
{
    private String appKey;
    
    private String appSecret;
    
    public RongCloudUser(String appKey, String appSecret)
    {
        this.appKey = appKey;
        this.appSecret = appSecret;
        
    }
    
    public RongCloudUser()
    {
        
    }
    
    public String getAppKey()
    {
        return appKey;
    }
    
    public void setAppKey(String appKey)
    {
        this.appKey = appKey;
    }
    
    public String getAppSecret()
    {
        return appSecret;
    }
    
    public void setAppSecret(String appSecret)
    {
        this.appSecret = appSecret;
    }
    
    
    public static void main(String[] args)
    {
        System.out.println();
    }
}