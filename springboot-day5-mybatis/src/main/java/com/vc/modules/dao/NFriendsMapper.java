package com.vc.modules.dao;

import com.vc.common.dao.BaseDao;
import com.vc.modules.entity.NFriends;

public interface NFriendsMapper extends BaseDao<NFriends>
{
}