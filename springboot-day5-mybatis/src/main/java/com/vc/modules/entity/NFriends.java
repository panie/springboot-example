package com.vc.modules.entity;

import com.vc.common.entity.DataEntity;
import com.vc.common.utils.JsonMapper;

public class NFriends extends DataEntity<NFriends>{
    private Integer id;

    private Integer userid;

    private Integer friendId;

    private String friendNick;

    private String friendNickEn;

    private String friendNickHead;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getFriendId() {
        return friendId;
    }

    public void setFriendId(Integer friendId) {
        this.friendId = friendId;
    }

    public String getFriendNick() {
        return friendNick;
    }

    public void setFriendNick(String friendNick) {
        this.friendNick = friendNick == null ? null : friendNick.trim();
    }

    public String getFriendNickEn() {
        return friendNickEn;
    }

    public void setFriendNickEn(String friendNickEn) {
        this.friendNickEn = friendNickEn == null ? null : friendNickEn.trim();
    }

    public String getFriendNickHead() {
        return friendNickHead;
    }

    public void setFriendNickHead(String friendNickHead) {
        this.friendNickHead = friendNickHead == null ? null : friendNickHead.trim();
    }
    
    public String toString()
    {
        return JsonMapper.getInstance().toJson(this);
    }
}