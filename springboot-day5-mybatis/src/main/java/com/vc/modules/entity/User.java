package com.vc.modules.entity;

import com.vc.common.entity.DataEntity;

public class User extends DataEntity<User>
{
    private Integer id;
    
    private String username;
    
    private Integer age;
    
    private String account;
    
    private String phonenum;
    
    private String password;
    
    private String idcard;
    
    private String face;
    
    public Integer getId()
    {
        return id;
    }
    
    public void setId(Integer id)
    {
        this.id = id;
    }
    
    public String getUsername()
    {
        return username;
    }
    
    public void setUsername(String username)
    {
        this.username = username == null ? null : username.trim();
    }
    
    public Integer getAge()
    {
        return age;
    }
    
    public void setAge(Integer age)
    {
        this.age = age;
    }
    
    public String getAccount()
    {
        return account;
    }
    
    public void setAccount(String account)
    {
        this.account = account == null ? null : account.trim();
    }
    
    public String getPhonenum()
    {
        return phonenum;
    }
    
    public void setPhonenum(String phonenum)
    {
        this.phonenum = phonenum == null ? null : phonenum.trim();
    }
    
    public String getPassword()
    {
        return password;
    }
    
    public void setPassword(String password)
    {
        this.password = password == null ? null : password.trim();
    }
    
    public String getIdcard()
    {
        return idcard;
    }
    
    public void setIdcard(String idcard)
    {
        this.idcard = idcard == null ? null : idcard.trim();
    }

    public String getFace()
    {
        return face;
    }
    
    public void setFace(String face)
    {
        this.face = face == null ? null : face.trim();
    } 
    
}