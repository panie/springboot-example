package com.vc.modules.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vc.modules.entity.User;
import com.vc.modules.service.UserService;

@RestController
@RequestMapping(value = "/user")
public class UserController
{
    @Autowired
    private UserService userService;
    
    /**
     * 查询所有用户信息
     * 
     * @param user
     * @return
     */
    @RequestMapping(value="/list",method={RequestMethod.GET,RequestMethod.POST})
    @ResponseBody
    public Object getFriendList(User entity)
    {
        return userService.getList(entity);
    }
    
    /**
     * 通过用户id 来查询该用户的好友列表
     * 
     * @param user
     * @return
     */
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public Object addFriend(@RequestBody User entity)
    {
        System.out.println(entity.toString());
        userService.save(entity);
        return "OK";
    }
    
}
