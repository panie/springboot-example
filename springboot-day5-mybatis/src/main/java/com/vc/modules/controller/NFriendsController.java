package com.vc.modules.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vc.common.entity.BaseResponse;
import com.vc.common.utils.CnToPy;
import com.vc.modules.entity.NFriends;
import com.vc.modules.service.NFriendsService;

@RestController
@RequestMapping(value = "/friend")
public class NFriendsController
{
    @Autowired
    private NFriendsService nFriendsService;
    
    /**
     * 通过用户id 来查询该用户的好友列表
     * 
     * @param user
     * @return
     */
    @RequestMapping("/list")
    @ResponseBody
    public Object getFriendList(NFriends entity)
    {
        return nFriendsService.getList(entity);
    }
    
    /**
     * 通过用户id 来查询该用户的好友列表
     * 
     * @param user
     * @return
     */
    @RequestMapping(value = "/addFriend", method = RequestMethod.POST)
    @ResponseBody
    public Object addFriend(@RequestBody NFriends friends)
    {
        System.out.println(friends.toString());
        String nick = friends.getFriendNick();
        if (nick != null && !"".equals(nick))
        {
            String nickEn = CnToPy.getPingYin(nick);
            friends.setFriendNickEn(nickEn);
            friends.setFriendNickHead("" + nickEn.charAt(0));
        }
        nFriendsService.save(friends);
        return "OK";
    }
    
}
