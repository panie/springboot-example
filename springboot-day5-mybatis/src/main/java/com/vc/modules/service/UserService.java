package com.vc.modules.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vc.common.service.BaseService;
import com.vc.modules.dao.UserMapper;
import com.vc.modules.entity.User;

@Service
@Transactional
public class UserService extends BaseService<UserMapper,User>
{
}