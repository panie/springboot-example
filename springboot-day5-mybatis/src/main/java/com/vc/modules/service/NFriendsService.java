package com.vc.modules.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vc.common.service.BaseService;
import com.vc.modules.dao.NFriendsMapper;
import com.vc.modules.entity.NFriends;

@Service
@Transactional
public class NFriendsService extends BaseService<NFriendsMapper,NFriends>
{
}