package io.rong;

import io.rong.messages.*;
import io.rong.models.*;
import io.rong.util.GsonUtil;

import java.io.Reader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * 一些api的调用示例
 */
public class Example2 {
	private static final String JSONFILE = Example2.class.getClassLoader().getResource("jsonsource").getPath()+"/";
	/**
	 * 本地调用测试
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		String appKey = "panie";//替换成您的appkey
		String appSecret = "kenie123";//替换成匹配上面key的secret
		
		Reader reader = null ;
		RongCloud rongCloud = RongCloud.getInstance(appKey, appSecret);
				
		
		System.out.println("************************User********************");
		// 获取 Token 方法 
		TokenResult userGetTokenResult = rongCloud.user.getToken("userId1", "username", "http://www.rongcloud.cn/images/logo.png");
		System.out.println("getToken:  " + userGetTokenResult.toString());
		
		// 刷新用户信息方法 
		CodeSuccessResult userRefreshResult = rongCloud.user.refresh("userId1", "username", "http://www.rongcloud.cn/images/logo.png");
		System.out.println("refresh:  " + userRefreshResult.toString());
		
		
	 }
}